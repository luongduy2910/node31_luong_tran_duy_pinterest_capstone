FROM node:18 AS builder

# Create app directory
WORKDIR /app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./
COPY prisma ./prisma/

# Install app dependencies
RUN npm install
# Generate prisma client, leave out if generating in `postinstall` script
RUN npx prisma generate

COPY . .

RUN npm run build


ARG NODE_ENV=production
ENV JWT_SECRET=nestjs_practice
ENV DATABASE_URL=mysql://root:Archie1337@@mysql_db:3309

EXPOSE 5050
CMD ["node", "dist/main"]