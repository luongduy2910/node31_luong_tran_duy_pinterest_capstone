import { ApiProperty } from "@nestjs/swagger";

export class ImageDto {
    @ApiProperty()
    ten_hinh : string; 
    @ApiProperty()
    duong_dan : string;
    @ApiProperty()
    mo_ta : string;
    @ApiProperty()
    nguoi_dung_id : number;
}

export class SavedImageDto {
    @ApiProperty()
    hinh_id : number;
    @ApiProperty()
    nguoi_dung_id : number;
    @ApiProperty()
    ngay_luu : Date; 

}