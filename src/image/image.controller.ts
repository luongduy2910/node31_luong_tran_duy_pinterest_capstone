import { Body, Controller, Delete, Get, Headers, Param, ParseIntPipe, Post, Query, Req, Request, UseGuards } from '@nestjs/common';
import { ImageService } from './image.service';
import { ImageDto, SavedImageDto } from './dto/image.dto';
import {ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { hinh_anh, luu_anh, nguoi_dung } from '@prisma/client';
import { AuthGuard } from '@nestjs/passport';

@ApiTags('image')
@Controller('image')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
export class ImageController {
    constructor(
        private readonly imageService : ImageService
    ){}
    @Get('get-image')
    getImage(
        @Query('ten_hinh') ten_hinh : string
    ) : Promise<ImageDto[]>{
        return this.imageService.getImage(ten_hinh);  
    }
    @Get('get-image/:hinh_id')
    getImageByID(
        @Param('hinh_id' , ParseIntPipe) hinh_id : number
    ){
        return this.imageService.getImageByID(hinh_id) ; 
    }
    @Get('check-savedimage/:hinh_id')
    checkSavedImage(
        @Param('hinh_id' , ParseIntPipe) hinh_id : number , 
        @Request() req 
    ){
        let data : nguoi_dung = req.user ; 
        return this.imageService.checkSavedImage(hinh_id , data) ;
    }
    @Get('get-createdimage/:nguoi_dung_id')
    getCreatedImage(
        @Param('nguoi_dung_id' , ParseIntPipe) nguoi_dung_id : number
    ) : Promise<hinh_anh[]>{
        return this.imageService.getCreatedImage(nguoi_dung_id) ; 
    }
    @Get('get-savedimage/:nguoi_dung_id')
    getSavedImage(
        @Param('nguoi_dung_id' , ParseIntPipe) nguoi_dung_id : number
    ) : Promise<luu_anh[]>{
        return this.imageService.getSavedImage(nguoi_dung_id)
    }
    @Post('create-image')
    @ApiBody({type : ImageDto})
    createImage(
        @Body() data : hinh_anh
    ){
        return this.imageService.createImage(data);
    }
    @Post('save-image')
    @ApiBody({type : SavedImageDto})
    saveImage(
        @Body() data : luu_anh
    ) : Promise<string>{
        return this.imageService.saveImage(data) ; 
    }
    @Delete('delete-image/:hinh_id')
    deleteImage(
        @Param('hinh_id' , ParseIntPipe) hinh_id : number , 
        @Req() req
    ) : Promise<string>{
        let data : nguoi_dung = req.user ; 
        return this.imageService.deleteImage(hinh_id , data) ; 
    }
}
