import { ForbiddenException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { ImageDto, SavedImageDto } from './dto/image.dto';
import { hinh_anh, luu_anh, nguoi_dung } from '@prisma/client';


@Injectable()
export class ImageService {
    constructor(
        private readonly prisma : PrismaService 
    ){}
    async getImage(ten_hinh : string) : Promise<ImageDto[]> {
        if(!ten_hinh){
            return await this.prisma.hinh_anh.findMany({
                include : {
                    nguoi_dung : {
                        select : {
                            ho_ten : true , 
                            tuoi : true ,
                        }
                    }
                }
            })
        }else {
            return this.prisma.hinh_anh.findMany({
                include : {
                    nguoi_dung : {
                        select : {
                            ho_ten : true ,
                            tuoi : true
                        }
                    }
                },
                where : {
                    ten_hinh : {
                        contains : ten_hinh
                    }
                }
            })
        }
    }
    async getImageByID(hinh_id : number) : Promise<ImageDto>{
        return this.prisma.hinh_anh.findUnique({
            where : {
                hinh_id
            }, 
            include : {
                nguoi_dung : {
                    select : {
                        nguoi_dung_id : true , 
                        ho_ten : true , 
                        tuoi : true ,
                    }
                }
            }
        })
    }
    async checkSavedImage(hinh_id : number , data : nguoi_dung ){
        let found = await this.prisma.luu_anh.findFirst({
            where : {
                hinh_id , 
                nguoi_dung_id : data.nguoi_dung_id
            }

        })
        if (!found) {
            throw new ForbiddenException('Người dùng chưa lưu ảnh')
        } else {
            return found
        }
    }
    async getCreatedImage(nguoi_dung_id : number) : Promise<hinh_anh[]> {
        return this.prisma.hinh_anh.findMany({
            where : {
                nguoi_dung_id
            }
        })
    }
    async getSavedImage(nguoi_dung_id : number) : Promise<luu_anh[]> {
        return this.prisma.luu_anh.findMany({
            where : {
                nguoi_dung_id
            }, 
            include : {
                hinh_anh : {
                    select : {
                        ten_hinh : true , 
                        duong_dan : true , 
                    }
                } , 
                nguoi_dung : {
                    select : {
                        ho_ten : true , 
                        tuoi : true , 
                    }
                }
            }
        })
    }
    async createImage(data : ImageDto) : Promise<string>{
        await this.prisma.hinh_anh.create({
            data : data
        })
        return 'Tạo ảnh thành công'
    }
    async saveImage(data : SavedImageDto) : Promise<string>{
        await this.prisma.luu_anh.create({
            data 
        })
        return 'Lưu ảnh thành công'
    }
    async deleteImage(hinh_id : number , data : nguoi_dung) : Promise<string>{
        await this.prisma.hinh_anh.deleteMany({
            where : {
                nguoi_dung_id :  data.nguoi_dung_id , 
                hinh_id
            }
        })
        return 'Xóa ảnh thành công'
    }
}
