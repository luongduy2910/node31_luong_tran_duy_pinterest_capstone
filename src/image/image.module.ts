import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ImageController } from './image.controller';
import { ImageService } from './image.service';
import { PrismaService } from 'src/prisma/prisma.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { MiddlewareModule } from 'src/middleware/middleware.module';
import { JwtService } from '@nestjs/jwt';
import { JwtStrategy } from 'src/strategy/jwt.strategy';

@Module({
  imports : [PrismaModule], 
  controllers: [ImageController],
  providers: [ImageService , JwtStrategy , JwtService]
})
export class ImageModule{}
