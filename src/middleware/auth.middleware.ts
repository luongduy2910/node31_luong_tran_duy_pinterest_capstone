import { ForbiddenException, Injectable, NestMiddleware } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";

@Injectable()
export class AuthMiddleware implements NestMiddleware{
    constructor(
        private readonly jwt : JwtService , 
        private readonly config : ConfigService
    ){}
    use(req: any, res: any, next: (error?: any) => void) {
        let {token} = req.headers ;  
        if(token){
            let checkToken = this.jwt.verify(token , {
                secret : this.config.get('JWT_SECRET'),
            }) ; 
            if (checkToken) {
                next();  
            } else {
                throw new ForbiddenException('Token không hợp lệ') ; 
            }
        }else {
            throw new ForbiddenException('Token chưa được cung cấp') ; 
        }
    }
}