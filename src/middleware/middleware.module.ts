import { MiddlewareConsumer, Module, NestMiddleware, NestModule } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { AuthMiddleware } from './auth.middleware';

@Module({
    providers : [JwtService , ConfigService], 
})
export class MiddlewareModule 
// implements NestMiddleware
{
    // configure(consumer: MiddlewareConsumer) {
    //     consumer.apply(AuthMiddleware).forRoutes('image' , 'user' , 'comment') ; 
    // }
}
