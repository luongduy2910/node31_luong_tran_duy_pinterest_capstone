import { Args, Query, Resolver } from "@nestjs/graphql";
import { PrismaService } from "src/prisma/prisma.service";
import { GImageDto } from "./dto/image.dto";
import { UseGuards } from "@nestjs/common";
import { GraphqlAuthGuard } from "src/middleware/graphql.guard";

@UseGuards(GraphqlAuthGuard)
@Resolver()
export class ImageResolver {
    constructor(
        private readonly prisma : PrismaService
    ){}
    @Query(returns => [GImageDto])
    async getImage() : Promise<GImageDto[]>{
        return this.prisma.hinh_anh.findMany({
            include : {
                nguoi_dung : true
            }
        }) ; 
    }
    @Query(returns => GImageDto)
    async getImageByID(
        @Args('hinh_id') hinh_id : number
    ) : Promise<GImageDto> {
        return this.prisma.hinh_anh.findUnique({
            where : {
                hinh_id
            }, 
            include : {
                nguoi_dung : true
            }
        })
    }
}