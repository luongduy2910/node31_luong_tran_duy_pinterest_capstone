import { Module } from '@nestjs/common';
import { UserResolver } from './user.resolver';
import { PrismaModule } from 'src/prisma/prisma.module';
import { ImageResolver } from './image.resolver';
import { CommentResolver } from './comment.resolver';
// import { GPostCommentDto } from './dto/comment.dto';
import { CommentModule } from 'src/comment/comment.module';
import { AuthResolver } from './auth.resolver';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports : [PrismaModule , CommentModule , AuthModule], 
  providers: [UserResolver , ImageResolver , CommentResolver , AuthResolver , JwtService , ConfigService ]
})
export class GraphqlModule {}
