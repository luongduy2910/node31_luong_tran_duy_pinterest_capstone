import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserDto, UserInfoDto } from 'src/auth/dto/auth.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { GLuuAnhDto, GUserDto } from './dto/user.dto';
import { GImageDto } from './dto/image.dto';
import { UseGuards } from '@nestjs/common';
import { GraphqlAuthGuard } from 'src/middleware/graphql.guard';

@UseGuards(GraphqlAuthGuard)
@Resolver(() => UserInfoDto)
export class UserResolver {
    constructor(
        private readonly prisma : PrismaService
    ){}
    @Query(returns => [GUserDto])
    async getUsers() : Promise<GUserDto[]> {
        return this.prisma.nguoi_dung.findMany() ; 
    }
    @Query(returns => GUserDto)
    async getUserByID(
        @Args('nguoi_dung_id') nguoi_dung_id : number 
    ) : Promise<GUserDto> {
        return await this.prisma.nguoi_dung.findUnique({
            where : {
                nguoi_dung_id
            }
        })
    }
    @Query(returns => [GLuuAnhDto])
    async getSavedImage(
        @Args('nguoi_dung_id') nguoi_dung_id : number
    ) : Promise<GLuuAnhDto[]>{
        return await this.prisma.luu_anh.findMany({
            where : {
                nguoi_dung_id
            } , 
            include : {
                nguoi_dung : true , 
                hinh_anh : true
            }
        })
    }
    @Query(returns => [GImageDto])
    async getCreatedImage(
        @Args('nguoi_dung_id') nguoi_dung_id : number
    ) : Promise<GImageDto[]>{
        return this.prisma.hinh_anh.findMany({
            where : {
                nguoi_dung_id
            } , 
            include : {
                nguoi_dung : true
            }
        })
    }
}
