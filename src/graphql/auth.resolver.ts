import { ConfigService } from "@nestjs/config";
import { Args, ArgsType, Mutation, Resolver } from "@nestjs/graphql";
import { JwtService } from "@nestjs/jwt";
import { PrismaService } from "src/prisma/prisma.service";
import * as bcrypt from 'bcrypt'
import { ForbiddenException } from "@nestjs/common";
import { TokenDto } from "src/auth/dto/auth.dto";
import { AuthService } from "src/auth/auth.service";
import { Token } from "src/auth/interface/auth.interface";
import { GAuthDto } from "./dto/auth.dto";
import { GUserDto } from "./dto/user.dto";

@Resolver() 
export class AuthResolver {
    constructor(
        private readonly prisma : PrismaService , 
        private readonly jwt : JwtService , 
        private readonly config : ConfigService , 
        private readonly authService : AuthService
    ){}

    @Mutation(returns => String)
    async signUp(
        @Args('email') email : string , 
        @Args('mat_khau') mat_khau : string , 
        @Args('ho_ten') ho_ten : string , 
        @Args('tuoi') tuoi : number , 
    ) : Promise<String>{
        let checkEmail = await this.prisma.nguoi_dung.findFirst({
            where : {
                email 
            }
        })
        if(checkEmail){
            throw new ForbiddenException('Email đã đồn tại')
        }else {
            await this.prisma.nguoi_dung.create({
                data : {
                    ho_ten , 
                    email , 
                    mat_khau : bcrypt.hashSync(mat_khau , 10) , 
                    tuoi , 

                }
            }); 
            return "Đăng kí thành công"
        }
    }
    @Mutation(returns => GAuthDto)
    async login(
        @Args('email') email : string , 
        @Args('mat_khau') mat_khau : string
    ) : Promise<GAuthDto>{
        let found = await this.prisma.nguoi_dung.findFirst({
            where : {
                email
            }
        })
        if (found && bcrypt.compareSync(mat_khau , found.mat_khau)){
            let token = await this.authService.generateToken({...found , mat_khau : ""}) ; 
            console.log(token);
            return token ; 
            
        }else {
            throw new ForbiddenException('Email or password incorrect') ; 
        }
    }
}