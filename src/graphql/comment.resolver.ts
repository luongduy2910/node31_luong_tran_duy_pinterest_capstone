import { Args, Mutation, Query, Resolver } from "@nestjs/graphql";
import { GBinhLuanDto  } from "./dto/comment.dto";
import { PrismaService } from "src/prisma/prisma.service";
import { CommentService } from "src/comment/comment.service";
import { UseGuards } from "@nestjs/common";
import { GraphqlAuthGuard } from "src/middleware/graphql.guard";

@UseGuards(GraphqlAuthGuard)
@Resolver(()=> GBinhLuanDto)
export class CommentResolver {
    constructor(
        private readonly prisma : PrismaService , 
        private readonly commentService : CommentService , 
    ){}
    @Query(returns => [GBinhLuanDto])
    async getComment(
        @Args('hinh_id') hinh_id : number
        ) : Promise<GBinhLuanDto[]>{
        return await this.prisma.binh_luan.findMany({
            where : {
                hinh_id
            } ,
            include : {
                hinh_anh : true , 
                nguoi_dung : true 
            }
        });
    }
    @Mutation(returns => GBinhLuanDto)
    async postComment(
        @Args('nguoi_dung_id') nguoi_dung_id : number ,
        @Args('hinh_id') hinh_id : number,
        @Args('noi_dung') noi_dung : string,
        @Args('ngay_binh_luan') ngay_binh_luan : Date,

        ){
            return await this.prisma.binh_luan.create({
                data : {
                    nguoi_dung_id , 
                    hinh_id , 
                    noi_dung , 
                    ngay_binh_luan
                }
            })
            
        }
        
    }