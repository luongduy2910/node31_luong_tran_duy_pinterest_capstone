import { ObjectType , Field , Int, InputType} from "@nestjs/graphql";
import { GImageDto } from "./image.dto";
import { GBinhLuanDto } from "./comment.dto";

@ObjectType()
export class GUserDto {
    @Field(() => Int)
    nguoi_dung_id: number;

    @Field({ nullable: true })
    email?: string;

    @Field({ nullable: true })
    mat_khau?: string;

    @Field({ nullable: true })
    ho_ten?: string;

    @Field(() => Int, { nullable: true })
    tuoi?: number;

    @Field({ nullable: true })
    anh_dai_dien?: string;

    @Field(() => [GBinhLuanDto], { nullable: true })
    binh_luan?: GBinhLuanDto[];

    @Field(() => [GImageDto], { nullable: true })
    hinh_anh?: GImageDto[];

    @Field(() => [GLuuAnhDto], { nullable: true })
    luu_anh?: GLuuAnhDto[];
}


@ObjectType()
export class GLuuAnhDto {
    @Field(() => Int)
    ban_luu_id: number;

    @Field(() => GUserDto, { nullable: true })
    nguoi_dung?: GUserDto;

    @Field(() => GImageDto, { nullable: true })
    hinh_anh?: GImageDto;

    @Field({ nullable: true })
    ngay_luu?: Date;
}
