import { Field, ObjectType , Int } from "@nestjs/graphql";
import { GUserDto } from "./user.dto";
import { GBinhLuanDto } from "./comment.dto";
import { GLuuAnhDto } from "./user.dto";

@ObjectType()
export class GImageDto {
  @Field(() => Int , {nullable : true})
  hinh_id: number;

  @Field({ nullable: true })
  ten_hinh?: string;

  @Field({ nullable: true })
  duong_dan?: string;

  @Field({ nullable: true })
  mo_ta?: string;

  @Field(() => GUserDto, { nullable: true })
  nguoi_dung?: GUserDto;

  @Field(() => [GBinhLuanDto], { nullable: true })
  binh_luan?: GBinhLuanDto[];

  @Field(() => [GLuuAnhDto], { nullable: true })
  luu_anh?: GLuuAnhDto[];
}

// @ObjectType()
// export class GImageDto {
//     @Field(type => Number)
//     hinh_id : number ; 
//     @Field(type => String)
//     ten_hinh : string ; 
//     @Field(type => String)
//     duong_dan : string ; 
//     @Field(type => String)
//     mo_ta : string ; 
//     @Field(type => Number)
//     nguoi_dung_id : Number ; 
//     @Field(type => GuserDto)
//     nguoi_dung?: GuserDto; 
// }

// @ObjectType()
// export class GImageDtoWithUser {
//     @Field(type => Number)
//     hinh_id : number ; 
//     @Field(type => String)
//     ten_hinh : string ; 
//     @Field(type => String)
//     duong_dan : string ; 
//     @Field(type => String)
//     mo_ta : string ; 
//     @Field(type => GuserDto)
//     nguoi_dung : GuserDto; 
// }