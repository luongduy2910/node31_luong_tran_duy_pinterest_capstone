import { Field, ObjectType } from "@nestjs/graphql";

@ObjectType() 
export class GAuthDto {
    @Field()
    accessToken : string;
}