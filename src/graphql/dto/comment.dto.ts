import { Field, ObjectType , Int , InputType } from "@nestjs/graphql";
import { GUserDto } from "./user.dto";
import { GImageDto } from "./image.dto";

@ObjectType()
export class GBinhLuanDto {
    @Field(() => Int)
    binh_luan_id: number;

    @Field(() => GUserDto, { nullable: true })
    nguoi_dung?: GUserDto;

    @Field(() => GImageDto, { nullable: true })
    hinh_anh?: GImageDto;

    @Field({ nullable: true })
    ngay_binh_luan?: Date;

    @Field({ nullable: true })
    noi_dung?: string;
}

@InputType()
export class GBinhLuanInput {
    @Field(() => Int  , {nullable : false})
    readonly nguoi_dung_id: number;

    @Field(() => Int , {nullable : false})
    readonly hinh_id: number;

    @Field(() => String , {nullable : false})
    readonly noi_dung: string;

    @Field(() => String , {nullable : false})
    readonly ngay_binh_luan: Date;
}

// @ObjectType() 
// export class GComment {
//     @Field()
//     binh_luan_id : number ; 

//     @Field()
//     nguoi_dung_id : number ;

//     @Field()
//     hinh_id : number; 

//     @Field()
//     noi_dung : string;

//     @Field()
//     ngay_binh_luan : Date ;
// }

// @ObjectType()
// export class GCommentWithRelation {
//     @Field() 
//     binh_luan_id : number; 
//     @Field(type => GImageDto) 
//     hinh_anh : GImageDto ; 
//     @Field(type => Number)
//     nguoi_dung_id : number;
//     @Field()
//     noi_dung : string;
//     @Field()
//     ngay_binh_luan : Date ;
// }
