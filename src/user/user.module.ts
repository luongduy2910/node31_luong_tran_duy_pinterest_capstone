import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtStrategy } from 'src/strategy/jwt.strategy';
import { JwtService } from '@nestjs/jwt';

@Module({
  imports : [PrismaModule] , 
  controllers: [UserController],
  providers: [UserService , JwtStrategy , JwtService]
})
export class UserModule {}
