import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsNotEmpty } from "class-validator";

export class uploadAvatarDto {
    @ApiProperty({type : 'file' , format : 'binary'})
    @IsNotEmpty()
    @Type(() => Object)
    file : Express.Multer.File
}