import { Injectable } from '@nestjs/common';
import { UserDto } from 'src/auth/dto/auth.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { uploadAvatarDto } from './dto/upload-avatar.dto';

@Injectable()
export class UserService {
    constructor(
        private readonly prisma : PrismaService
    ){}
    async getUserByID(nguoi_dung_id : number) : Promise<UserDto>{
        return await this.prisma.nguoi_dung.findUnique({
            where : {
                nguoi_dung_id
            }
        }) ;
        
    }
    async uploadAvatar(nguoi_dung_id : number , fileUpload : Express.Multer.File) : Promise<string>{
        let user = await this.prisma.nguoi_dung.findFirst({
            where : {
                nguoi_dung_id 
            }
        }) ; 
        user.anh_dai_dien = fileUpload.filename ; 
        await this.prisma.nguoi_dung.update({
            data : user , 
            where : {
                nguoi_dung_id
            }
        })
        return fileUpload.filename ; 
    }
}
