import { Controller , Get, Param, ParseIntPipe, Post, Request, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from 'src/auth/dto/auth.dto';
import { ApiConsumes, ApiTags , ApiBearerAuth , ApiBody } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { AuthGuard } from '@nestjs/passport';
import { uploadAvatarDto } from './dto/upload-avatar.dto';
import { ApiFile } from 'src/upload-file.decorator';

@ApiTags('user')
@ApiBearerAuth()
@UseGuards(AuthGuard('jwt'))
@Controller('user')
export class UserController {
    constructor(
        private readonly userService : UserService
    ){}
    @Get('get-user/:nguoi_dung_id')
    getAllUser(
        @Param('nguoi_dung_id' , ParseIntPipe) nguoi_dung_id : number 
    ) : Promise<UserDto>{
        return this.userService.getUserByID(nguoi_dung_id) ; 
    }
    @ApiConsumes('multipart/form-data')
    @ApiFile('file')
    @UseInterceptors(FileInterceptor('file' , {
        storage : diskStorage({
            destination : process.cwd() + "/public/img" , 
            filename : (req , file , callback) => {
                let date = new Date() ; 
                callback(null , date.getTime() + "_" + file.originalname)
            }
        })
    }))
    @Post('upload-avatar')
    uploadAvatar(
        @UploadedFile() fileUpload : Express.Multer.File ,
        @Request() req 
    ) : Promise<string>{
        let {nguoi_dung_id}  = req.user 
        return this.userService.uploadAvatar(nguoi_dung_id , fileUpload) ; 
    }
}
