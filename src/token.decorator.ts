import { applyDecorators, SetMetadata } from '@nestjs/common';
import { ApiHeader } from '@nestjs/swagger';

export const TokenHeader = (tokenName = "accessToken"): any =>
    applyDecorators(
        ApiHeader({
        name: tokenName,
        required: true,
        }),
        SetMetadata('tokenName', 'accessToken'),
    );