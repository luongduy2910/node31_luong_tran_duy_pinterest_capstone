import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express'; 
import { ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin : '*'
  }) ; 
  app.use(express.static('.')) ; 
  app.useGlobalPipes(new ValidationPipe({whitelist : true})) ; 
  const config = new DocumentBuilder()
  .setTitle('Pinterest-Capstone')
  .setDescription('API description')
  .setVersion('1.0')
  .addBearerAuth()
  .build();
  const document = SwaggerModule.createDocument(app , config) ; 
  SwaggerModule.setup('api' , app , document , {
    swaggerOptions : {
      persistAuthorization: true,
    },
  }) ; 
  await app.listen(5050);
}
bootstrap();
