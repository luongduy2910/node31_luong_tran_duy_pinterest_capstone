import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { ImageModule } from './image/image.module';
import { MiddlewareModule } from './middleware/middleware.module';
import { UserModule } from './user/user.module';
import { CommentModule } from './comment/comment.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver} from '@nestjs/apollo'
import { GraphqlModule } from './graphql/graphql.module';
import { GraphqlAuthGuard } from './middleware/graphql.guard';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal : true,
    }),
    PrismaModule,
    AuthModule,
    ImageModule,
    MiddlewareModule,
    UserModule,
    CommentModule,
    GraphQLModule.forRoot({
      autoSchemaFile : 'schema.gql', 
      driver : ApolloDriver , 
    }),
    GraphqlModule
  ],
  controllers: [AppController],
  providers: [AppService , GraphqlAuthGuard],
})
export class AppModule {}
