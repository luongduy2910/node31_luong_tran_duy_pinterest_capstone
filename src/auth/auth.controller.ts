import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthDto, UserDto } from './dto/auth.dto';
import { Token } from './interface/auth.interface';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService : AuthService
    ){}
    @Post('sign-up')
    signUp(
        @Body() userDto : UserDto 
    ) : Promise<string>{
        return this.authService.signUp(userDto); 
    }
    @Post('login')
    login(
        @Body() authDto : AuthDto
    ) : Promise<Token>{
        return this.authService.login(authDto) ; 
    }
    
}
