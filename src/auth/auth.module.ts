import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Module({
  imports : [PrismaModule] , 
  controllers: [AuthController],
  providers: [AuthService , JwtService , ConfigService] , 
  exports : [AuthService]
})
export class AuthModule {}
