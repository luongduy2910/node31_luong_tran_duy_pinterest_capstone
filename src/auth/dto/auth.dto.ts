import { Field, ObjectType } from "@nestjs/graphql";
import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class AuthDto {
    @ApiProperty()
    @IsEmail()
    email : string;
    @ApiProperty()
    @IsString()
    mat_khau : string;
}

export class UserDto {
    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email : string;
    @ApiProperty()
    @IsString()
    mat_khau : string;
    @ApiProperty()
    @IsString()
    ho_ten : string;
    @ApiProperty()
    @IsNumber()
    tuoi : number; 
    @ApiProperty()
    @IsString()
    anh_dai_dien : string;
}

export class UserInfoDto {

    nguoi_dung_id : number ; 

    email : string;

    mat_khau : string;

    ho_ten : string;

    tuoi : number; 

    anh_dai_dien : string;
}

@ObjectType()
export class TokenDto {
    @Field()
    token : string
}