import { ForbiddenException, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PrismaService } from 'src/prisma/prisma.service';
import {AuthDto, UserDto } from './dto/auth.dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt'; 
import { Token } from './interface/auth.interface';

@Injectable()
export class AuthService {
    constructor(
        private readonly prisma : PrismaService ,
        private readonly config : ConfigService , 
        private readonly jwt : JwtService
    ){}
    async signUp(userDto : UserDto) : Promise<string>{
        let checkEmail = await this.prisma.nguoi_dung.findFirst({
            where : {
                email : userDto.email
            }
        })
        if(!checkEmail){
            await this.prisma.nguoi_dung.create({
                data : {
                    email : userDto.email , 
                    mat_khau : bcrypt.hashSync(userDto.mat_khau , 12) ,
                    anh_dai_dien : userDto.anh_dai_dien , 
                    ho_ten : userDto.ho_ten , 
                    tuoi : userDto.tuoi
                }
            })
            return "Đăng kí thành công"
        }else {
            return "Email đã có người sử dụng"
        }
    }
    async login(authdto : AuthDto) : Promise<Token>{
        let checkEmail = await this.prisma.nguoi_dung.findFirst({
            where : {
                email : authdto.email
            }
        }); 
        if(checkEmail && bcrypt.compareSync(authdto.mat_khau , checkEmail.mat_khau)){
            let token = this.generateToken({...checkEmail , mat_khau : ""});  
            return token
        }else {
            throw new ForbiddenException('Email or Password incorrect'); 
        }
    }
    async generateToken(payload : UserDto) : Promise<Token>{
        payload = {...payload , mat_khau : ""};  
        let token = await this.jwt.signAsync(payload , {
            expiresIn : '35m', 
            secret : this.config.get('JWT_SECRET')
        })
        return {accessToken : token}
    }
}
