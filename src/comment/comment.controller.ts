import { Controller, Param, ParseIntPipe  ,Get, Post, Body, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { binh_luan } from '@prisma/client';
import { ApiBearerAuth, ApiCreatedResponse, ApiTags, getSchemaPath  } from '@nestjs/swagger';
import { ApiBody } from '@nestjs/swagger';
import { CommentDto } from './dto/comment.dto';
import { AuthGuard } from '@nestjs/passport';


@ApiTags('comment')
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('comment')
export class CommentController {
    constructor(
        private readonly commentService: CommentService
    ){}
    @Get('get-comment/:hinh_id')
    getComment(
        @Param('hinh_id' , ParseIntPipe) hinh_id : number
    ) : Promise<binh_luan[]>{
        return this.commentService.getCommentByID(hinh_id) ; 
    }
    @Post('post-comment')
    @ApiBody({type : CommentDto})
    postComment(
        @Body() comment : binh_luan, 
    ) : Promise<string> {
        return this.commentService.postComment(comment) ; 
    }
}
