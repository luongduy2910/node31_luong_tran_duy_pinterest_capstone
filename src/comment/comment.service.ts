import { Injectable } from '@nestjs/common';
import { binh_luan } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';

@Injectable()
export class CommentService {
    constructor(
        private readonly prisma : PrismaService
    ){}
    async getCommentByID(hinh_id : number) : Promise<binh_luan[]>{
        return this.prisma.binh_luan.findMany({
            where : {
                hinh_id
            }, 
            include : {
                hinh_anh : {
                    select : {
                        ten_hinh : true , 
                        duong_dan : true ,
                        hinh_id : true
                    }
                } , 
                nguoi_dung : {
                    select : {
                        ho_ten : true ,
                        nguoi_dung_id : true
                    }
                }
            }
        })
    }
    async postComment(comment : binh_luan) : Promise<string> {
        await this.prisma.binh_luan.create({
            data : {...comment , ngay_binh_luan :  new Date(comment.ngay_binh_luan)}
        })
        return "Đăng bình luận thành công" ; 
    }
}
