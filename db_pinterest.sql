-- -------------------------------------------------------------
-- TablePlus 5.3.8(500)
--
-- https://tableplus.com/
--
-- Database: db_pinterest
-- Generation Time: 2023-06-18 20:47:49.8200
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


CREATE TABLE `binh_luan` (
  `binh_luan_id` int NOT NULL AUTO_INCREMENT,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  `ngay_binh_luan` datetime DEFAULT NULL,
  `noi_dung` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`binh_luan_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  KEY `hinh_id` (`hinh_id`),
  CONSTRAINT `binh_luan_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `binh_luan_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hinh_anh` (
  `hinh_id` int NOT NULL AUTO_INCREMENT,
  `ten_hinh` varchar(255) DEFAULT NULL,
  `duong_dan` varchar(255) DEFAULT NULL,
  `mo_ta` varchar(255) DEFAULT NULL,
  `nguoi_dung_id` int DEFAULT NULL,
  PRIMARY KEY (`hinh_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  CONSTRAINT `hinh_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `luu_anh` (
  `ban_luu_id` int NOT NULL AUTO_INCREMENT,
  `nguoi_dung_id` int DEFAULT NULL,
  `hinh_id` int DEFAULT NULL,
  `ngay_luu` date DEFAULT NULL,
  PRIMARY KEY (`ban_luu_id`),
  KEY `nguoi_dung_id` (`nguoi_dung_id`),
  KEY `hinh_id` (`hinh_id`),
  CONSTRAINT `luu_anh_ibfk_1` FOREIGN KEY (`nguoi_dung_id`) REFERENCES `nguoi_dung` (`nguoi_dung_id`),
  CONSTRAINT `luu_anh_ibfk_2` FOREIGN KEY (`hinh_id`) REFERENCES `hinh_anh` (`hinh_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `nguoi_dung` (
  `nguoi_dung_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `mat_khau` varchar(255) DEFAULT NULL,
  `ho_ten` varchar(255) DEFAULT NULL,
  `tuoi` int DEFAULT NULL,
  `anh_dai_dien` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`nguoi_dung_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `binh_luan` (`binh_luan_id`, `nguoi_dung_id`, `hinh_id`, `ngay_binh_luan`, `noi_dung`) VALUES
(1, 1, 3, '2023-02-10 08:50:00', 'Ảnh đẹp quá đi mất'),
(2, 7, 1, '2022-01-11 15:00:00', 'Tôi rất thích biệt đội avengers!!!'),
(3, 1, 2, '2023-04-14 09:53:00', 'Captain America quá đẹp trai cool ngầu'),
(4, 7, 4, '2023-02-02 21:05:00', 'Naruto là thần tượng từ bé của tôi'),
(5, 1, 2, '2023-02-10 02:50:00', 'captain number 1 !!!'),
(6, 8, 5, '2023-01-10 02:00:00', 'boruto và kawaki , ai win đây mọi người'),
(7, 13, 1, '2023-03-20 02:00:00', 'Avengers là thần tượng thời thanh xuân của tôi'),
(98, 16, 1, '2022-12-24 00:00:00', 'Biệt đội siêu ngầu'),
(105, 16, 2, '2023-03-08 00:00:00', 'captain đẹp trai'),
(138, 16, 3, '2022-10-30 00:00:00', 'Naruto và Hinata đẹp đôi quá');

INSERT INTO `hinh_anh` (`hinh_id`, `ten_hinh`, `duong_dan`, `mo_ta`, `nguoi_dung_id`) VALUES
(1, 'avengers', 'https://images.immediate.co.uk/production/volatile/sites/3/2019/04/Avengers-Endgame-Banner-2-de7cf60.jpg?quality=90&crop=93px,0px,1013px,675px&resize=980,654', 'Biệt đội siêu anh hùng cool ngầu', 1),
(2, 'captain america', 'https://www.sideshow.com/storage/product-images/904929/captain-america-2012-version_marvel_square.jpg', 'Biểu tượng anh hùng nước Mỹ', 1),
(3, 'naruto', 'https://m.media-amazon.com/images/M/MV5BMDI3ZDY4MDgtN2U2OS00Y2YzLWJmZmYtZWMzOTM3YWFjYmUyXkEyXkFqcGdeQXVyNjAwNDUxODI@._V1_FMjpg_UX1000_.jpg', 'Người tình muôn đời của sasuke', 7),
(4, 'sasuke', 'https://static.wikia.nocookie.net/naruto-viet-nam/images/d/d7/1_%282%29.jpg/revision/latest?cb=20170430070958&path-prefix=vi', 'Đẹp trai nhưng có tính trẩu', 7),
(5, 'boruto', 'https://w0.peakpx.com/wallpaper/289/711/HD-wallpaper-boruto-naruto-next-generation-anime-naruto-shippuden-boruto.jpg', 'Thế hệ tiếp theo của bộ truyện naruto', 9),
(11, 'Asta', 'https://gamek.mediacdn.vn/133514250583805952/2020/6/28/photo-1-1593389411220915741711.jpg', 'Ma pháp vương trong tương lai', 13);

INSERT INTO `luu_anh` (`ban_luu_id`, `nguoi_dung_id`, `hinh_id`, `ngay_luu`) VALUES
(1, 1, 4, '2023-06-01'),
(2, 8, 1, '2022-12-24'),
(3, 1, 2, '2023-03-08'),
(4, 13, 11, '2023-06-12');

INSERT INTO `nguoi_dung` (`nguoi_dung_id`, `email`, `mat_khau`, `ho_ten`, `tuoi`, `anh_dai_dien`) VALUES
(1, 'duy1@gmail.com', '$2b$10$a5UHbQO0CmaabCVBiWhX0uEdsJDH5D8QkFXIlbOGvLZZuYrduRld.', 'Lương Trần Duy', 23, '1686483474089_cena.jpeg'),
(7, 'nghia1@gmail.com', '$2b$10$Ei6yh7NmvihZtWcO5XeHc.gwnKHB.6oHQMwSeGqoCPN/ComUX1Koq', 'Hiếu Nghĩa', 21, 'https://www.facebook.com/photo/?fbid=2379400758954532&set=a1392669307627687'),
(8, 'duy2@gmail.com', '$2b$10$lJI8Ve6UTYru9sfdtw6Sou5yp7.dD/a0MwvIQeR.S6ganKrAiNvmS', 'Trần Duy', 25, 'https://www.facebook.com/photo/?fbid=2379400758954532&set=a1392669307627687'),
(9, 'nguyena@gmail.com', '$2b$10$vQf3T/kEr7RawQlyvi.BVOKFtuoP8FEVVIU0fo3gGauUrG3kQiEyS', 'Nguyễn Văn A', 20, 'https://sm.ign.com/ign_ap/cover/a/avatar-gen/avatar-generations_hugw.jpg'),
(10, 'test7@gmail.com', '$2b$10$LVFiEHUJKTXaHzaoeHa1HeOQkh7uF.sDBaIWwY6UVgMrsxvgJxekS', 'Ronaldo', 38, 'https://danviet.mediacdn.vn/296231569849192448/2023/6/2/ronaldo-01-1685673247129749947581.jpeg'),
(12, 'test10@gmail.com', '$2b$12$ayhFn3Yw4/i8llC5zlerWejS/CHaTRGbm40uEL1J5YVMBZHH8r6OG', 'Messi', 36, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQi1xUGv3FUL_SAny5_7ok7aANVSnCgSbBN9A&usqp=CAU'),
(13, 'quochuy@gmail.com', '$2b$12$.uDzd1AvllZwHcLhvW1pHOdG9lgmnx4zsk/xGo8JVVZiE0WijDcri', 'Võ Quốc Huy', 21, '1686490560092_captain.webp'),
(14, 'ronaldo7@gmail.com', 'ronaldo', 'Cristiano Ronaldo', 38, 'https://encrypted-tbn2.gstatic.com/licensed-image?q=tbn:ANd9GcS6jS4xWr-ip4_p7okt-VUk8LvjTi-kDCLv3qbGvrC-QSsMZHMm3zCTaAf4n4eU5f0dtZQnkhB_5g6Qujs'),
(15, 'messi10@gmail.com', '$2b$10$TB1eKqu54N6njELLkUxL0.CT/ElSybsoHhEKMAhMJHVTi2r9iBgLG', 'Lion Messi', 36, 'https://encrypted-tbn0.gstatic.com/licensed-image?q=tbn:ANd9GcRX_cJsmG-pyDff1Xyr7l3FrSHoWqhJD577T120CoA90Y4NYnQsS2kWRPLWaY_JtMKNTlcqclP8gZoDfiE'),
(16, 'thanhnhi@gmail.com', '$2b$12$WjwlmcQ6D3Nez4kJqWF9OuFJREZUweFJbCfQxmln0FOEvE6toUHJu', 'Thanh Nhi', 21, ''),
(17, 'truong1@gmail.com', '$2b$12$fjaS8eqdkM5ha1qdY/7yKuTGcJs4MdTinjl/Us/5xA7InBdygYNB.', 'Trường', 20, ''),
(19, 'hoangphuoc1@gmail.com', '$2b$10$TKo9U7u79WGbqsG7u6vCcucq/sCq/PwmqfKfG1faoSApFlq7k3s4S', 'Võ Hoàng Phước', 21, NULL),
(20, 'phuc12@gmail.com', '$2b$10$fVVgqBcvBpaVQ1TCZYbibeXp4.BSIyfBKQ5vrwYjmI6F3ccf1X.x6', 'Phúc', 27, NULL),
(21, 'nguyen1@gmail.com', '$2b$10$jWdJzYZEaUpO4c7bZlg1Ou2lpTlXK.3BmvoiGKvMGht2SOVzQohGi', 'Nguyen Ngoc Nguyen', 24, NULL);



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;